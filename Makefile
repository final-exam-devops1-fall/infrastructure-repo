.PHONY: init plan apply destroy

init:
    terraform init -backend-config="address=https://gitlab.com/final-exam-devops1-fall/infrastructure-repo" -backend-config="token=glpat-WYmYpY5-pXqyxRMWxi2F" -backend-config="project=53134373" -backend-config="path=https://gitlab.com/final-exam-devops1-fall/infrastructure-repo/-/blob/terraform/gitlab-runner/gitlab-runner/terraform.tfstate"

plan:
    terraform plan

apply:
    terraform apply

destroy:
    terraform destroy
