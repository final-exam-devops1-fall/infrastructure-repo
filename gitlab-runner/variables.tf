variable "location" {
  type        = string
  description = "This is location our server"
}

variable "username" {
  type        = string
  description = "This is username my pc"
}

variable "ssh_key_path" {
  type        = string
  description = "This is my path ssh key public" 
}

variable "ssh_secret_key_path" {
  type        = string
  description = "This is my path ssh key sercet" 
}

variable "my_ami" {
  type        = string
  description = "This is my server ami" 
}

variable "instance_type" {
  type        = string
  description = "This is type our server" 
}

# gitlab runner

variable gitlab_token {
  type        = string
  sensitive = true
  description = "Access token"
}

variable "gitlab_url" {
  type        = string
  description = "URL of the GitLab CI/CD server"
}

variable "gitlab_registration_token" {
  type        = string
  sensitive = true
  description = "Registration token for the GitLab Runner"
}

variable "group_id" {
  type = number
  description = "This is id of our group"
}