output "ip_vm" {
  value = aws_instance.finalServer.public_ip
}

output "gitlab_runner_toml_file" {
 value = module.runner-register.runner_config_toml
 sensitive = true
}