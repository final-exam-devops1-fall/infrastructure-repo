resource "null_resource" "gitlab-runner" {

  triggers = {
    gitlab_registration_token = var.gitlab_registration_token
  }

  provisioner "local-exec" {
    command = <<EOF
      sudo gitlab-runner register \
        --non-interactive \
        --url "https://gitlab.com/" \
        --registration-token "${var.gitlab_registration_token}" \
        --executor "docker" \
        --docker-image "ruby:2.7" \
        --description "terraform-registered-runner v2 defend" \
        --run-untagged="true" \
        --locked="false" \
        --access-level="not_protected"
      sudo gitlab-runner verify
    EOF
  }
}

data "local_file" "gitlab_runner_config" {
  depends_on = [null_resource.gitlab-runner]
  filename = "${path.module}/config.toml"
}

output "gitlab_runner_config_contents" {
  value = data.local_file.gitlab_runner_config.content
}
