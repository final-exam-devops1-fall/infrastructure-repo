variable "gitlab_token" {
  description = "This is access key gitlab"
  type = string
}

variable "gitlab_registration_token" {
  type = string
}